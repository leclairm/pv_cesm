# Calculate (Vertically Averaged) Potential Vorticity for CESM

Python wrapper for fortran code calculating PV and vertical integration of PV.

## Source
* The fortran code was written by Daniel Luethi, IAC.

## Installation

* get the source code with git
* use pip: `pip install /path/to/pv_cesm`
* or: `pip install /path/to/pv_cesm --user`

## Usage

```python
import pv_cesm

ifile = 'path/to/file.nc'

# calculate PV
# PV = pv_cesm.calc_pv(ifile)
# this may result in an error if the 3D field is very large

# calculate VAPV
VAPV = pv_cesm.calc_vapv(ifile, 15000, 50000)

# easiest way to save 
import xarray as xr

ds = xr.open_dataset(ifile)

ds = ds.PS
ds.values[:] = VAPV

ds = ds.to_dataset(name='VAPV')
ds.to_netcdf('VAPV.nc', format="NETCDF4_CLASSIC")

```

## Requirements

The input file needs to contain the following variables: `lat`, `lon`, 
`time`, `hyam`, `hybm`, `P0`, `PS`, `U`, `V`, `T`

## Compilation

~~The fortran code must be compiled with f2py:~~ The fortran code is now
compiled by the setup.py script/ pip. I leave this here for reference.

    f2py pv_fortran.f -m pv_fortran -h pv_fortran.pyf
    f2py -c pv_fortran.pyf pv_fortran.f

## NCL

The NCL routine to calculate the same is also given in the folder NCL. Usage:
 

    ncl 'ifile=''ifile.nc''' 'ofile=''ofile.nc'' VAPV.ncl

Functions:

* [Potential Vorticity](http://www.ncl.ucar.edu/Document/Functions/Contributed/pot_vort_hybrid.shtml)
* [Vertical Integration](http://www.ncl.ucar.edu/Document/Functions/Built-in/vibeta.shtml)

## Test

Compares the output of the NCL routines and the python/ fortran routine.
Note that they will not give exactly the same answer as the algorithm 
(derivatives) are different.

Usage:

	# to test the file in py_cesm/tst_data/tst.nc
    python test.py
    # to test an arbitrary file
    python test.py /path/to/test/file.nc



Mathias Hauser, 2016