      subroutine potvort(pv,uu,vv,th,sp,cl,f,ie,je,ke,ak,bk,
     >     vmin,vmax)
C     ======================================================
C     calculate PV
 
c     argument declaration
      integer   ie,je,ke
      real, intent(out) :: pv(ie,je,ke)
      real uu(ie,je,ke),vv(ie,je,ke),
     >     th(ie,je,ke),sp(ie,je),
     >     cl(ie,je),f(ie,je)
      real      ak(ke),bk(ke),vmin(4),vmax(4)
      real      pvpole
 
c     variable declaration
      REAL, DIMENSION (ie, je) :: dspdx,dspdy
      REAL, DIMENSION (ie,je,ke) :: dthdp,dudp,dvdp
      REAL, DIMENSION (ie,je,ke) :: uu2,dvdx,dudy,dthdx,dthdy
      integer   i,j,k,stat
 
 
      call ddp(th,dthdp,sp,ie,je,ke,ak,bk)
      call ddp(uu,dudp,sp,ie,je,ke,ak,bk)
      call ddp(vv,dvdp,sp,ie,je,ke,ak,bk)
      call ddh2(sp,dspdx,cl,'X',ie,je,1,vmin,vmax)
      call ddh2(sp,dspdy,cl,'Y',ie,je,1,vmin,vmax)
      call ddh3(th,dthdx,sp,dspdx,cl,'X',ie,je,ke,vmin,vmax,ak,bk)
      call ddh3(vv,dvdx,sp,dspdx,cl,'X',ie,je,ke,vmin,vmax,ak,bk)
      call ddh3(th,dthdy,sp,dspdy,cl,'Y',ie,je,ke,vmin,vmax,ak,bk)
c     conversion of uu for spheric coordinates (uu*cos(phi))
      do k=1,ke
         uu2(1:ie,1:je,k)=uu(1:ie,1:je,k)*cl(1:ie,1:je)
      enddo
      call ddh3(uu2,dudy,sp,dspdy,cl,'Y',ie,je,ke,vmin,vmax,ak,bk)
c     conversion of dudy for spheric coordinates (dudy/cos(phi))
      do k=1,ke
         dudy(1:ie,1:je,k)=dudy(1:ie,1:je,k)/cl(1:ie,1:je)
      enddo
 
      do k=1,ke
         pv(1:ie,1:je,k)=1.E6*9.80665*(
     >        -(-dudy(1:ie,1:je,k)+dvdx(1:ie,1:je,k)
     >        +f(1:ie,1:je))*dthdp(1:ie,1:je,k)
     >        -(dudp(1:ie,1:je,k)*dthdy(1:ie,1:je,k)
     >        -dvdp(1:ie,1:je,k)*dthdx(1:ie,1:je,k)))
      enddo
 
c     interpolate poles from neighbouring points on every level
      if (vmax(2).gt.89.5) then
        do k=1,ke
          pvpole=0.
          do i=1,ie
            pvpole=pvpole+pv(i,je-1,k)
          enddo
          pvpole=pvpole/real(ie)
          do i=1,ie
            pv(i,je,k)=pvpole
          enddo
        enddo
      endif
 
      if (vmin(2).lt.-89.5) then
        do k=1,ke
          pvpole=0.
          do i=1,ie
            pvpole=pvpole+pv(i,2,k)
          enddo
          pvpole=pvpole/real(ie)
          do i=1,ie
            pv(i,1,k)=pvpole
          enddo
        enddo
      endif
 
 
      end


      subroutine vint(intfield,field,sp,pmin,pmax,ie,je,ke,ak,bk)
C     ===========================================================

c     argument declaration
      integer   ie,je,ke
      real, intent(out) :: intfield(ie,je)
      real      field(ie,je,ke),sp(ie,je)
      real      ak(ke),bk(ke)
      real      pmin,pmax

c     variable declaration
      integer   i,j,k,kmin,kmax
      real      coeff,pval

c     statement-functions for the computation of pressure
      real      pp,psrf
      integer   is
      pp(is)=ak(is)+bk(is)*psrf

C     ====================================================================
C     ===  begin of main part of this subroutine  ========================
C     ====================================================================

      do i=1,ie
      do j=1,je
        intfield(i,j)=0.
        psrf=sp(i,j)
        if (psrf.lt.pmin) then                     
          intfield(i,j)=-999.98999
          goto 556
        endif
        kmin=1
        kmax=1
        do k=2,ke
          if ((pp(k).lt.pmax).and.(pp(k-1).gt.pmax)) kmin=k
          if ((pp(k).lt.pmin).and.(pp(k-1).gt.pmin)) kmax=k-1
        enddo
c       check if field is not equal mdv
        do k=kmin,kmax+1
          if (field(i,j,k).eq.-999.98999) then
            intfield(i,j)=-999.98999
            goto 556
          endif
        enddo
c       pmin and pmax are inbetween same pair of layers
c       interpolate on intermediate value (pmin+pmax)/2.
        if (kmin.eq.kmax+1) then
          pval=(pmin+pmax)/2.
          coeff=(pval-pp(kmin-1))/(pp(kmin)-pp(kmin-1))
          intfield(i,j)=(pmax-pmin)*
     &      (field(i,j,kmin)*coeff+field(i,j,kmax)*(1.-coeff))
          goto 555
        endif
c       one layer is inbetween pmin and pmax
        if (kmin.eq.kmax) then
          if (psrf.lt.pmax) then
            intfield(i,j)=field(i,j,kmin)*(psrf-pmin)
          else
            intfield(i,j)=field(i,j,kmin)*(pmax-pmin)
          endif
          goto 555
        endif
c       loop for the interior levels
        do k=kmin+1,kmax-1
          intfield(i,j)=intfield(i,j)+field(i,j,k)*
     &              0.5*(pp(k-1)-pp(k+1))
        enddo
c       special treatment of the bounding levels
        if (kmin.eq.1) then
          if (psrf.lt.pmax) then
            intfield(i,j)=intfield(i,j)+
     &          field(i,j,1)*(0.5*(pp(1)-pp(2))+psrf-pp(1))
          else
            intfield(i,j)=intfield(i,j)+
     &          field(i,j,1)*(0.5*(pp(1)-pp(2))+pmax-pp(1))
          endif
        else
          coeff=(pmax-pp(kmin-1))/(pp(kmin)-pp(kmin-1))
            intfield(i,j)=intfield(i,j)+
     &      field(i,j,kmin)*0.5*(pmax-pp(kmin+1))+
     &      (field(i,j,kmin)*coeff+field(i,j,kmin-1)*(1.-coeff))*
     &      0.5*(pmax-pp(kmin))
        endif
        if (kmax.eq.ke) then          
          intfield(i,j)=intfield(i,j)+
     &           field(i,j,ke)*0.5*(pp(ke-1)-pp(ke))
        else
          coeff=(pmin-pp(kmax+1))/(pp(kmax)-pp(kmax+1))
          intfield(i,j)=intfield(i,j)+
     &      field(i,j,kmax)*0.5*(pp(kmax-1)-pmin)+
     &      (field(i,j,kmax)*coeff+field(i,j,kmax+1)*(1.-coeff))*
     &      0.5*(pp(kmax)-pmin)
        endif
  555   continue
C       Calculate mean value
        if (psrf.lt.pmax) then
          intfield(i,j)=intfield(i,j)/(psrf-pmin)
        else
          intfield(i,j)=intfield(i,j)/(pmax-pmin)
        endif
  556   continue
      enddo
      enddo

      return
      end


      subroutine ddp(a,d,sp,ie,je,ke,ak,bk)
c-----------------------------------------------------------------------
c     Purpose: VERTICAL DERIVATIVE
c     Compute the vertical derivative without missing data checking.
c     The derivative is taken from array 'a' in the direction of 'P'.
c     The result is stored in array 'd'.
c     3 point weighted centered differencing is used.
c     The vertical level-structure of the data is of the form
c     p=ak(k)+bk(k)*ps.
c-----------------------------------------------------------------------
 
c     declaration of arguments
      integer       ie,je,ke
      real          a(ie,je,ke),sp(ie,je)
      real, intent(out) :: d(ie,je,ke)
 
c     variable declaration
      integer       i,j,k
      real          dpu,dpl,quot,fac,psrf
      real          ak(ke),bk(ke)
 
c     specify scaling factor associated with derivation with respect
c     to pressure
      fac=0.01
 
c     compute vertical 3 point derivative
c     ---------------------------
c     3-point vertical derivative
c     ---------------------------
      do j=1,je
         do i=1,ie
c     get surface pressure at current grid-point
            psrf=sp(i,j)
c     points at k=1
            dpu=(ak(1)+bk(1)*psrf)-(ak(2)+bk(2)*psrf)
            d(i,j,1)=(a(i,j,1)-a(i,j,2))*fac/dpu
c     points at 1<k<ke
            do k=2,ke-1
               dpu=(ak(k)+bk(k)*psrf)-(ak(k+1)+bk(k+1)*psrf)
               dpl=(ak(k-1)+bk(k-1)*psrf)-(ak(k)+bk(k)*psrf)
               quot=dpu/dpl
               d(i,j,k)=(quot*(a(i,j,k-1)-a(i,j,k))
     &              +1./quot*(a(i,j,k)-a(i,j,k+1)))*fac/(dpu+dpl)
            enddo
c     points at k=ke
            dpl=(ak(ke-1)+bk(ke-1)*psrf)-(ak(ke)+bk(ke)*psrf)
            d(i,j,ke)=(a(i,j,ke-1)-a(i,j,ke))*fac/dpl
         enddo
      enddo
      end
 
 
      subroutine ddh3(a,d,ps,dps,cl,dir,ie,je,ke,datmin,datmax,ak,bk)
c-----------------------------------------------------------------------
c     Purpose: HORIZONTAL DERIVATIVE ON PRESSURE-SURFACES WITHOUT
c     MISSING DATA
c     The derivative is taken from array 'a' in the direction of 'dir',
c     where 'dir' is either 'X','Y'. The result is stored in array 'd'.
c     The routine accounts for derivatives at the pole and periodic
c     boundaries in the longitudinal direction (depending on
c     the value of datmin, datmax). If the data-set does not reach to
c     the pole, a one-sided derivative is taken. Pole-treatment is only
c     carried out if the data-set covers 360 deg in longitude, and it
c     requires that ie=4*ii+1, where ii is an integer.
c     History:
c     Daniel Luethi
c-----------------------------------------------------------------------
 
c     declaration of arguments
      integer       ie,je,ke
      real          a(ie,je,ke),cl(ie,je)
      real, intent(out) :: d(ie,je,ke)
      real          ps(ie,je),dps(ie,je)
      real          datmin(4),datmax(4)
      character*(*) dir
 
c     variable declaration
      integer       i,j,k
      real          ak(ke),bk(ke),as(100),bs(100)
 
c     compute vertical derivatives of ak's and bk's
      do k=2,ke-1
         as(k)=(ak(k-1)-ak(k+1))/2.
         bs(k)=(bk(k-1)-bk(k+1))/2.
      enddo
      as(1 )=ak(1)-ak(2)
      bs(1 )=bk(1)-bk(2)
      as(ke)=ak(ke-1)-ak(ke)
      bs(ke)=bk(ke-1)-bk(ke)
 
c     compute horizontal derivatives on sigma surfaces
      call ddh2(a,d,cl,dir,ie,je,ke,datmin,datmax)
 
c     apply correction for horizontal derivative on p-surfaces
      do j=1,je
         do i=1,ie
            do k=2,ke-1
               d(i,j,k)=d(i,j,k)+bk(k)*dps(i,j)/2./(as(k)+
     &              bs(k)*ps(i,j))*(a(i,j,k+1)-a(i,j,k-1))
            enddo
            k=1
            d(i,j,k)=d(i,j,k)+bk(k)*dps(i,j)/(as(k)+
     &           bs(k)*ps(i,j))*(a(i,j,k+1)-a(i,j,k))
            k=ke
            d(i,j,k)=d(i,j,k)+bk(k)*dps(i,j)/(as(k)+
     &           bs(k)*ps(i,j))*(a(i,j,k)-a(i,j,k-1))
         enddo
      enddo
      end
 
 
      subroutine ddh2(a,d,cl,dir,ie,je,ke,datmin,datmax)
c-----------------------------------------------------------------------
c     Purpose: HORIZONTAL DERIVATIVE ON DATA-SURFACES WITHOUT MISSING
C     DATA
c     Compute the horizontal derivative without missing data checking.
c     The derivative is taken from array 'a' in the direction of 'dir',
c     where 'dir' is either 'X','Y'. The result is stored in array 'd'.
c     The routine accounts for derivatives at the pole and periodic
c     boundaries in the longitudinal direction (depending on
c     the value of datmin, datmax). If the data-set does not reach to
c     the pole, a one-sided derivative is taken. Pole-treatment is only
c     carried out if the data-set covers 360 deg in longitude, and it
c     requires that ie=4*ii+1, where ii is an integer.
c-----------------------------------------------------------------------
 
c     declaration of arguments
      integer       ie,je,ke
      real          a(ie,je,ke),cl(ie,je)
      real, intent(out) :: d(ie,je,ke)
      real          datmin(4),datmax(4)
      character*(*) dir
 
c     local variable declaration
      integer       i,j,k,ip1,im1,jp1,jm1,ip,im,j1,j2
      real          dlat,dlon,coslat,dx,dy,dxr,dyr
      integer       northpl,southpl,lonper
 
c     rerd and circ are the mean radius and diameter of the earth in
c     meter
      real          rerd,circ,pi
      data          rerd,circ,pi /6.37e6,4.e7,3.141592654/
 
c     compute flags for pole and periodic treatment
      southpl=0
      northpl=0
      lonper =0
      j1=1
      j2=je
      if (abs(datmax(1)-datmin(1)-360.).lt.1.e-3) then
         lonper=1
         if (abs(datmin(2)+90.).lt.1.e-3) then
            southpl=1
            j1=2
         endif
         if (abs(datmax(2)-90.).lt.1.e-3) then
            northpl=1
            j2=je-1
         endif
      endif
 
      dlon=((datmax(1)-datmin(1))/float(ie-1)) *pi/180.
      dlat=((datmax(2)-datmin(2))/float(je-1)) *pi/180.
 
c     print *,'Computing derivative ',dir(1:1),
c     &        ' of an array dimensioned ',ie,je,ke
 
      if (dir(1:1).eq.'X') then
c     -----------------------------
c     derivation in the x-direction
c     -----------------------------
         do k=1,ke
 
c     do gridpoints at j1<=j<=j2
            do j=j1,j2
               coslat=cl(1,j)
 
c     do regular gridpoints at 1<i<ie, 1<j<je
               dx =rerd*coslat*dlon
               dxr=1./(2.*dx)
               do i=2,ie-1
                  ip1=i+1
                  im1=i-1
                  d(i,j,k)=dxr*(a(ip1,j,k)-a(im1,j,k))
               enddo            ! i-loop
c     completed regular gridpoints at 1<i<ie, 1<j<je
 
c     do gridpoints at i=1, i=ie, 1<j<je
               if (lonper.eq.1) then
c     use periodic boundaries
                  i=1
                  ip1=2
                  im1=ie-1
                  d(i,j,k)=dxr*(a(ip1,j,k)-a(im1,j,k))
                  d(ie,j,k)=d(1,j,k)
               else
c     use one-sided derivatives
                  dxr=1./dx
                  i=1
                  ip1=2
                  im1=1
                  d(i,j,k)=dxr*(a(ip1,j,k)-a(im1,j,k))
                  i=ie
                  ip1=ie
                  im1=ie-1
                  d(i,j,k)=dxr*(a(ip1,j,k)-a(im1,j,k))
               endif
c     completed gridpoints at i=1, i=ie, j1<=j<=j2
 
            enddo               ! j-loop
c     completed gridpoints at 1<j<je
 
c     do gridpoints at j=je
            if (northpl.eq.1) then
c     for these gridpoints, the derivative in the x-direction is a
c     derivative in the y-direction at another pole-gridpoint
               dy =rerd*dlat
               dyr=1./(2.*dy)
               j=je
               jp1=je-1
               jm1=je-1
               do i=1,ie
                  ip=mod(i-1+  (ie-1)/4,ie)+1
                  im=mod(i-1+3*(ie-1)/4,ie)+1
                  d(i,j,k)=dyr*(a(ip,jp1,k)-a(im,jm1,k))
               enddo            ! i-loop
c     completed gridpoints at j=je
            endif
c     do gridpoints at j=1
            if (southpl.eq.1) then
               dy =rerd*dlat
               dyr=1./(2.*dy)
               j=1
               jp1=2
               jm1=2
               do i=1,ie
                  ip=mod(i-1+  (ie-1)/4,ie)+1
                  im=mod(i-1+3*(ie-1)/4,ie)+1
                  d(i,j,k)=dyr*(a(ip,jp1,k)-a(im,jm1,k))
               enddo            ! i-loop
            endif
c     completed gridpoints at j=1
 
         enddo                  ! k-loop
 
      else if (dir(1:1).eq.'Y') then
c     -----------------------------
c     derivation in the y-direction
c     -----------------------------
         dy =dlat*rerd
         dyr=1./(2.*dy)
         do k=1,ke
            do i=1,ie
 
c     do regular gridpoints
               do j=2,je-1
                  jp1=j+1
                  jm1=j-1
                  d(i,j,k)=dyr*(a(i,jp1,k)-a(i,jm1,k))
               enddo
 
c     do gridpoints at j=je
               if (northpl.eq.1) then
c     pole-treatment
                  j=je
                  jm1=j-1
                  jp1=j-1
                  ip=mod(i-1+(ie-1)/2,ie)+1
                  im=i
                  d(i,j,k)=dyr*(a(ip,jp1,k)-a(im,jm1,k))
               else
c     one-sided derivative
                  j=je
                  jm1=j-1
                  jp1=j
                  d(i,j,k)=2.*dyr*(a(i,jp1,k)-a(i,jm1,k))
               endif
c     completed gridpoints at j=je
 
c     do gridpoints at j=1
               if (southpl.eq.1) then
c     pole-treatment
                  j=1
                  jm1=2
                  jp1=2
                  ip=i
                  im=mod(i-1+(ie-1)/2,ie)+1
                  d(i,j,k)=dyr*(a(ip,jp1,k)-a(im,jm1,k))
               else
c     one-sided derivative
                  j=1
                  jm1=1
                  jp1=2
                  d(i,j,k)=2.*dyr*(a(i,jp1,k)-a(i,jm1,k))
               endif
c     completed gridpoints at j=1
 
            enddo
         enddo
 
      endif
      end
 
