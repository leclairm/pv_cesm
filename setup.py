from numpy.distutils.core import Extension, setup

from pkg_resources import resource_filename

fort = resource_filename('pv_cesm', 'pv_cesm/pv_fortran.f')


extension = Extension("pv_fortran", ['pv_cesm/pv_fortran.f'])
# extensions = cythonize(e)

if __name__ == '__main__':
	

    setup(
        name="pv_cesm",
        #version=george.__version__,
        author="Mathias Hauser",
        #author_email="",
        #url="",
        #license="",
        packages=["pv_cesm"],
        ext_modules=[extension],
        description="PV and VAPV for CESM.",
        #long_description=open("README.rst").read(),
        package_data={"": ["README.md", fort ]},
        include_package_data=True,
)








